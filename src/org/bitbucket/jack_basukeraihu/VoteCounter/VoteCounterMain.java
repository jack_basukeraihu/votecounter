package org.bitbucket.jack_basukeraihu.VoteCounter;

import java.util.logging.Logger;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.plugin.PluginDescriptionFile;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

public class VoteCounterMain extends JavaPlugin {
	public Logger logger = Logger.getLogger("Mineraft");
	String pex = ChatColor.GREEN + "[VoteCounter] ";

	public void onEnable() {
		saveDefaultConfig();
		if (!Bukkit.getPluginManager().isPluginEnabled("Votifier")) {
            getLogger().warning("Votifierが読み込まれていません プラグインを停止します");
            setEnabled(false);
            return;
        }
		PluginManager pm = getServer().getPluginManager();
		pm.registerEvents(new VoteListener(this), this);
		PluginDescriptionFile yml = getDescription();
		this.logger.info("[" + yml.getName() + "] v" + yml.getVersion()+ " が有効になりました");
	}

	public void onDisable() {
		PluginDescriptionFile yml = getDescription();
		this.logger.info("[" + yml.getName() + "] v" + yml.getVersion()+ " が無効になりました");
	}

	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if (cmd.getName().equalsIgnoreCase("votecounter")) {
			if (args.length == 0) {
				PluginDescriptionFile yml = getDescription();
				sender.sendMessage(pex+ChatColor.LIGHT_PURPLE+ "-=-=-[VoteCounter Ver" + yml.getVersion()+ "]-=-=-");
				sender.sendMessage(pex+ChatColor.RED+ "/"+label+" reload :configファイルを再読み込みします");
				sender.sendMessage(pex+ChatColor.RED+ "/"+label+" show <Service> :指定したサービスでの投票数を表示します");
				sender.sendMessage(pex+ChatColor.RED+ "/"+label+" show all :全てのサービスでの投票数と合計投票数を表示します");
				return true;
			}
			if (args.length == 1) {
				if (args[0].equalsIgnoreCase("reload")) {
					if (sender.hasPermission("votecounter.reload")) {
						reloadConfig();
						sender.sendMessage(pex+ChatColor.YELLOW+"Configファイルを再読み込みしました");
					} else {
						sender.sendMessage(pex+ChatColor.RED+"このコマンドを実行する権限がありません");
					}
				}
				if (args[0].equalsIgnoreCase("show")) {
					sender.sendMessage(pex+ChatColor.RED+"コマンドの形式が正しくありません");
					sender.sendMessage(pex+ChatColor.RED+ "/"+label+" show <Service> :指定したサービスでの投票数を表示します");
					sender.sendMessage(pex+ChatColor.RED+ "/"+label+" show all :全てのサービスでの投票数と合計投票数を表示します");
				}
			}
			if (args.length == 2) {
				if (args[0].equalsIgnoreCase("show")) {
					if (args[1].equalsIgnoreCase("all")) {
						if (sender.hasPermission("votecounter.show.all")) {
							int all = 0;
							for (String str : getConfig().getConfigurationSection("VoteCounter").getKeys(false)) {
								int vote = getConfig().getInt("VoteCounter."+str);
								sender.sendMessage(pex+ChatColor.GOLD+"サービス:"+str+" 投票数:"+vote);
								all = all +vote;
							}
							sender.sendMessage(pex+ChatColor.GOLD+"合計投票数: "+all);
						} else {
							sender.sendMessage(pex+ChatColor.RED+"このコマンドを実行する権限がありません");
						}
						return true;
					}

					if (sender.hasPermission("votecounter.show."+args[1])) {
						if (getConfig().contains("VoteCounter."+args[1])) {
							int vote = getConfig().getInt("VoteCounter."+args[1]);
							sender.sendMessage(pex+ChatColor.GOLD+"サービス:"+args[1]+" 投票数:"+vote);
						} else {
							sender.sendMessage(pex+ChatColor.RED+"指定されたサービスからの投票はありません");
						}
					} else {
						sender.sendMessage(pex+ChatColor.RED+"このコマンドを実行する権限がありません");
					}
				}
			}
		}
		return false;
	}


}
