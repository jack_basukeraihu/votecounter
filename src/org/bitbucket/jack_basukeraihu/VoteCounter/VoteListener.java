package org.bitbucket.jack_basukeraihu.VoteCounter;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

import com.vexsoftware.votifier.model.Vote;
import com.vexsoftware.votifier.model.VotifierEvent;

public class VoteListener implements Listener {
	public static VoteCounterMain plugin;

	public VoteListener(VoteCounterMain instance) {
		plugin = instance;
	}

	@EventHandler
	public void VoteEvent(VotifierEvent e) {
		Vote v = e.getVote();
		if (v.getServiceName() == null || v.getServiceName().equalsIgnoreCase("")) {
			plugin.logger.warning("[VoteCounter] サービス名が不明なサービスから投票されました VoteCounterにはカウントされません");
			return;
		}
		if (v.getUsername() == null || v.getUsername().equalsIgnoreCase("")) {
			if (!plugin.getConfig().getBoolean("AllowUnknownUsername")) {
				plugin.logger.warning("[VoteCounter] ユーザー名が不明なサービスから投票されました AllowUnknownUsernameが無効になっているため、VoteCounterにはカウントされません");
				return;
			}
		}
		if (plugin.getConfig().getStringList("DisableName").contains(v.getUsername())) {
			plugin.logger.warning("[VoteCounter] DisableNameに指定されている"+v.getUsername()+"から投票されました VoteCounterにはカウントされません");
			return;
		}

		if (plugin.getConfig().contains("VoteCounter."+v.getServiceName())) {
			int vote = plugin.getConfig().getInt("VoteCounter."+v.getServiceName());
			plugin.getConfig().set("VoteCounter."+v.getServiceName(), (vote+1));
		} else {
			plugin.getConfig().set("VoteCounter."+v.getServiceName(), 1);
		}
		plugin.saveConfig();
		plugin.logger.info("[VoteCounter] "+v.getUsername()+"が"+v.getServiceName()+"から投票を行いました");

	}

}
